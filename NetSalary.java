import java.util.Scanner;
public class NetSalary {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner (System.in);
		System.out.print("Enter your profit :");
				double profit = scan.nextFloat();
				double tax = 0;
				double salary = 0;
				
				if(profit >= 0 && profit <= 100000){		
    				System.out.println("Your salary is "+ profit +" Rs");
    			}
				
    			else if(100000 < profit && profit <= 150000){
    					tax = profit * 0.01 * 4 - 4000;
    					salary = profit - tax ;
    					
    				System.out.println("Your salary is " + salary  +" Rs");
    			}
				
    			else if(150000 < profit && profit <= 200000){
						tax = profit * 0.01 * 8 - 10000;
						salary = profit - tax ;
			
					System.out.println("Your salary is " + salary  +" Rs");
    			}
				
    			else if(200000 < profit && profit <= 250000){
    					tax = profit * 0.01 * 12 - 18000;
    					salary = profit - tax;

					System.out.println("Your salary is " + salary  +" Rs");
    			}
				
    			else if(250000 < profit && profit <= 300000){
    					tax = profit * 0.01 * 16 - 28000;
    					salary = profit - tax ;
					
					System.out.println("Your salary is " + salary  +" Rs");
    			}
				
    			else if(300000 < profit && profit <= 350000){
    					tax = profit * 0.01 * 20 - 40000;
    					salary = profit - tax ;
				
					System.out.println("Your salary is " + salary +" Rs");
    			}
				
    			else if(350000 < profit ){
    					tax = profit * 0.01 * 24 - 54000;
    					salary = profit - tax ;
	
					System.out.println("Your salary is " + salary  +" Rs");
    			}
				
    			else {
    				System.out.println("Invalid profit");
    			}

	}

}
